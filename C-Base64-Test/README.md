# C-Base64-Test

#### 介绍
本项目主要总结记录**C语言实现Base64编解码**的实用例程

#### 开发环境

本项目在如下环境中测试通过：

- Win10系统
- Dev C++ 开发环境

#### 项目结构

#####  01Demo

[Base64编解码Demo程序，熟悉Base64编解码的基本原理](01Demo/main.c)

- 字母（字符串）`P`的Base64编码原理

![](./pic/base64-p.png)

- 字符串`PC`的Base64编码原理

![](./pic/base64-pc.png)

- 字符串`PCB`的Base64编码原理

![](./pic/base64-pcb.png)

#### 推荐学习

- 我的B站：<https://space.bilibili.com/146899653>
- 个人博客：<https://xxpcb.gitee.io>
- 知乎：<https://www.zhihu.com/people/xxpcb>
- CSDN：https://blog.csdn.net/hbsyaaa
- 微信公众文章：![](../pic/wxgzh.png)

感谢支持~

