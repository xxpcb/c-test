/**
  ******************************************************************************
  * @file    main.c
  * @version V1.0
  * @date    2021-xx-xx
  * @brief   Base64编解码基础测试
  ******************************************************************************
  * 公众号  :码农爱学习(Coder-love-study)
  * 个人博客:https://xxpcb.gitee.io
  ******************************************************************************
  */ 
#include <stdio.h>
#include <string.h>

/*base64符号表*/
const char *base64Arr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
/*base64增补符号*/ 
const char paddingChar = '=';

/** @func:  base64_encode
*   @brief: base64编码 
*   @para:  [srcData]:要进行编码的原始数据
*           [resBase64]:base64编码结果 
*   @return:none
*/
void base64_encode(const unsigned char * srcData, char * resBase64)
{
    int i=0; /*原始数据索引*/ 
	int j=0; /*base64结果索引*/ 
    unsigned char transIdx=0;    // 索引是8位，但是高两位都为0
    const int srcLen = strlen((const char*)srcData);
    
    /*每3个一组，进行编码*/
    for(i=0; i < srcLen; i+=3) 
	{
        /*取出第1个字符的高6位*/ 
        transIdx = ((srcData[i] >> 2) & 0x3f); /*0011 1111*/
        /*查表*/ 
        resBase64[j++] = base64Arr[(int)transIdx];
        
        /*取出第1个字符的低2位*/ 
        transIdx = ((srcData[i] << 4) & 0x30); /*0011 0000*/
        
        /*第1个字符后面还有字符*/ 
        if (i + 1 < srcLen) 
		{
			/*取出第2个字符的高4位，并与第1个字符的低2位进行组合*/ 
            transIdx |= ((srcData[i + 1] >> 4) & 0x0f); /*0000 1111*/
            /*查表*/ 
            resBase64[j++] = base64Arr[(int)transIdx];
        }
		else /*第1个字符后面没有字符了*/
		{
			/*直接使用第1个字符的低2位查表*/
            resBase64[j++] = base64Arr[(int)transIdx];
            
            /*然后补上两个=号*/ 
            resBase64[j++] = paddingChar;
            resBase64[j++] = paddingChar;
            break;   /*没有数据了，break结束*/ 
        }
        
        /*取出第2个字符的低4位*/ 
        transIdx = ((srcData[i + 1] << 2) & 0x3c); /*0011 1100*/
        
        /*第2个字符后面还有字符*/ 
        if (i + 2 < srcLen)
		{ 
		    /*取出第3个字符的高2位，并与第2个字符的低4位进行组合*/ 
            transIdx |= ((srcData[i + 2] >> 6) & 0x03);  /*0000 0011*/
            /*查表*/ 
            resBase64[j++] = base64Arr[(int)transIdx];
            
            /*取出第3个字符的低6位*/ 
            transIdx = srcData[i + 2] & 0x3f; /*0011 1111*/ 
            /*查表*/ 
            resBase64[j++] = base64Arr[(int)transIdx];
        }
        else /*第2个字符后面没有字符了*/
		{
			/*直接使用第2个字符的低4位查表*/
            resBase64[j++] = base64Arr[(int)transIdx];
            
            /*然后补上一个=号*/ 
            resBase64[j++] = paddingChar;
            break;  /*没有数据了，break结束*/ 
        }
    }
    
    /*结束符*/ 
    resBase64[j] = '\0'; 
}


/** @func:  idx_in_base64Arr
*   @brief: 在base64符号表中查找字符c对应的索引值
*   @para:  [c]:要查找的字符
*   @return:字符c在base64符号表中对应的索引值(0~63) 
*/
int idx_in_base64Arr(char c) 
{
	/*在base64表中搜索第一次出现字符c的位置*/
    const char *pIdx = strchr(base64Arr, c);
    if (NULL == pIdx)
	{
		/*找不到对应的base64字符，说明输入的base64字符串有误*/ 
        return -1;
    }
    
    /*返回字符c在base64表中的位置*/
    return (pIdx - base64Arr);
}   

/** @func:  base64_decode
*   @brief: base64解码 
*   @para:  [srcBase64]:要进行解码的原始base64数据
*           [resData]:解码出的结果 
*   @return:none
*/
void base64_decode(const char *srcBase64, unsigned char *resData)
{
    int i = 0; /*原始base64数据索引*/ 
	int j = 0; /*解码后的结果数据索引*/ 
    int trans[4] = {0,0,0,0}; /*4个base64符号对应的表中的位置（0~63的数字）转换值*/ 
    
    /*base64符号每4个一组，译码成3个字符*/
    for (i=0; srcBase64[i]!='\0'; i+=4)
	{
		/*------译码第1个字符------*/
		/*前2个base64符号在表中的位置（0~63的数字）*/
        trans[0] = idx_in_base64Arr(srcBase64[i]);
        trans[1] = idx_in_base64Arr(srcBase64[i+1]);
        
		/*第1个符号的后6位，与第2个符号的6、5位，译出第1个字符*/ 
        resData[j++] = ((trans[0] << 2) & 0xfc) | ((trans[1]>>4) & 0x03); /*1111 1100   0000 0011 */ 

        /*------译码第2个字符------*/  
        /*第3个base64符号是否是=号*/
        if (srcBase64[i+2] != '=')
		{
			/*第3个base64符号在表中的位置（0~63的数字）*/
            trans[2] = idx_in_base64Arr(srcBase64[i + 2]);
        }
        else
		{
            break;/*没有数据了，break结束*/ 
        }
        /*第2个符号的后4位，与第3个符号的6、5、4、3位，译出第2个字符*/ 
        resData[j++] = ((trans[1] << 4) & 0xf0) | ((trans[2] >> 2) & 0x0f); /*1111 0000  0000 1111*/
  
        /*------译码第3个字符------*/
        /*第4个base64符号是否是=号*/
        if (srcBase64[i + 3] != '=')
		{
			/*第4个base64符号在表中的位置（0~63的数字）*/
			trans[3] = idx_in_base64Arr(srcBase64[i + 3]);
        }
        else
		{
            break;/*没有数据了，break结束*/ 
        }
        /*第3个符号的后2位，与第4个符号的后6位，译出第3个字符*/ 
        resData[j++] = ((trans[2] << 6) & 0xc0) | (trans[3] & 0x3f); /*1100 0000   0011 1111*/ 
    }
  
    /*结束符*/ 
    resData[j] = '\0';
}


/*测试*/
int main()
{
	/*定义要进行base64编码的字符串*/ 
    const unsigned char *srcData = "PCB" ;
    
    /*先测试编码*/
    char base64[128];
    base64_encode(srcData, base64);  
    printf("base64编码：%s\n",base64);

    /*再测试解码*/ 
    char resData[128];
    base64_decode(base64, (unsigned char*)resData);
    printf("base64解码：%s", resData);

    return 0;
}
