#include <stdio.h>
#define N 100

int main ()
{ 
    /*打开*/
    FILE *fp = fopen("../test-futs.txt","wt+");/*打开一个文件*/
    if(NULL == fp)
    {
        printf("open file fail\r\n");
        goto end;
    }
    
    /*写入*/
    fputs("hello world\n", fp);/*先写入一段信息*/
    fseek( fp, 6, SEEK_SET );/*读写位置从开头向后移动6个位置*/
    fputs("xxpcb.github.io\n", fp);/*再写入一段信息*/
    if(0 != fputs("码农爱学习\n", fp));/*再写入一段信息*/
    {
    	printf("fputs file ok\r\n");
	}

    
    /*关闭*/
    fclose(fp);
    
    /*再打开*/
    fp = fopen("../test-futs.txt","rt");
    if(NULL == fp)
    {
        printf("open file fail\r\n");
        goto end;
    }
    
    /*读取*/
    printf("fgets file ...\r\n");
    char str[N+1];
    int line = 0; 
    while(fgets(str, N, fp) != NULL)/*使用while, 可以一行一行的获取*/
    {
        printf("[%d]:%s", ++line, str);
    }
    
end:
   return(0);
}
