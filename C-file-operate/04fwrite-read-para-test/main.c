#include <stdio.h>
#include <stdlib.h>
int main()
{
	FILE *fp;
	
	//fp = fopen("../test-para.bin", "wb"); /*这种方式读文件失败*/ 
	fp = fopen("../test-para.bin", "wb+"); /*这种方式读文件成功*/ 
	if(NULL == fp)
    {
        printf("open file fail\r\n");
        goto end;
    }
	
    /*写入一些内容*/
    char *testData = "coder-love-study";/*测试数据*/
    size_t dataLen = strlen(testData);
    printf("dataLen:%d\r\n",dataLen);
    size_t cnt1 = fwrite(testData, 1, dataLen, fp);
    if(dataLen != cnt1)
    {
        printf("write file fail\r\n");
        fclose(fp);
        goto end;
    }
	
	fseek(fp, 0, SEEK_END);/*读写位置移到末尾*/ 
	long fileSize = ftell(fp);/*获取文件的长度*/
	rewind(fp);/*读写位置再移到开头*/ 
	
	/*根据文件的长度分配一块内存*/ 
	char* buf = (char*)malloc(sizeof(char) * fileSize);
	
	/*读取*/ 
	size_t cnt2 = fread(buf, 1, fileSize, fp);
	if (cnt2 != fileSize) 
	{
		printf("read file fail\r\n");
		fclose(fp);
		free(buf);
		goto end;
	}
	printf("read file:\r\n");
	printf("%s\r\n", buf); 
	fclose(fp);
	free(buf);

end:
	return 0;
} 
