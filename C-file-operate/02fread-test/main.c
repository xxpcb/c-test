#include <stdio.h>
#include <stdlib.h>

#define DATA_SIZE 100
 
int main()
{
    int pData[DATA_SIZE];
    
    /*打开*/
    FILE *fp = fopen("../test1.bin","rb");/*写入的是bin, 读取的也必须是bin*/
    if(NULL == fp)
    {
        printf("open file fail\r\n");
        goto end;
    }
    
    /*读取*/
    size_t cnt = fread(pData, sizeof(int), DATA_SIZE, fp);
    if(DATA_SIZE != cnt)
    {
        printf("read file fail, read size:%d\r\n", cnt);
        fclose(fp);
        goto end;
    }
    printf("data[30]:%d\r\n", pData[30]);/*打印出其中一个数据*/
    
    /*关闭*/
    fclose(fp);
    printf("file read ok\r\n");
    
 end:
    system("pause");
    
    return 0;
}
