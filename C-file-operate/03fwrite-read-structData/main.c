#include <stdio.h>
#include <stdlib.h>

typedef struct
{
	char name[256];
	unsigned int page;
	float price;	
}BOOK;
 
int main()
{
    BOOK book1 = {
        .name = "C���Ի���",
        .page = 320,
	    .price = 25.0,
    };
    
    /***********************д����***************************/
    /*��*/
    FILE *fp1 = fopen("../test2.bin","wb");
    if(NULL == fp1)
    {
        printf("open file fail\r\n");
        goto end;
    }
    
    /*д��*/
    size_t cnt1 = fwrite(&book1, 1, sizeof(BOOK), fp1);
    if(sizeof(BOOK) != cnt1)
    {
        printf("write file fail\r\n");
        fclose(fp1);
        goto end;
    }
    
    /*�ر�*/
    fclose(fp1);
    printf("file write ok\r\n");
    
    /***********************������***************************/
    BOOK myBook;
    
    /*��*/
    FILE *fp2 = fopen("../test2.bin","rb");
    if(NULL == fp2)
    {
        printf("open file fail\r\n");
        goto end;
    }
    
    /*��ȡ*/
    size_t cnt2 = fread(&myBook, 1, sizeof(BOOK), fp2);
    if(sizeof(BOOK) != cnt2)
    {
        printf("read file fail\r\n");
        fclose(fp2);    
        goto end;
    }
    printf("myBook info: name:%s, page:%d, price:%.2f\r\n",
	            myBook.name, myBook.page, myBook.price);
    
    /*�ر�*/
    fclose(fp2);
    printf("file read ok\r\n");
    
 end:
    system("pause");
    
    return 0;
}
