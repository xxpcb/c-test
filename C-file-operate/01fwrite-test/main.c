#include <stdio.h>
#include <stdlib.h>

#define DATA_SIZE 100
 
int main()
{
    unsigned char *pRawData = NULL;
    int *pData = NULL;
    
    /*使用malloc申请一块内存*/
    pRawData = (unsigned char *)malloc(sizeof(int) * DATA_SIZE);
    /*将该块内存强制转换为unsigned int*/
    pData = (int *) pRawData;
    
    /*对该块内存进行赋值 */
    int i = 0;
    for(i=0; i<DATA_SIZE; i++)
    {
        pData[i] = i; 
    }
    
    /*打开*/
    FILE *fp = fopen("../test1.bin","wb");
    if(NULL == fp)
    {
        printf("open file fail\r\n");
        goto end;
    }
    
    /*写入*/
    size_t cnt = fwrite(pData, sizeof(int), DATA_SIZE, fp);
    if(DATA_SIZE != cnt)
    {
        printf("write file fail\r\n");
        fclose(fp);
        goto end;
    }
    
    /*关闭*/
    fclose(fp);
    printf("file write ok\r\n");
    
 end:
    free(pRawData);/*malloc用完后要free*/
    system("pause");
    
    return 0;
}
