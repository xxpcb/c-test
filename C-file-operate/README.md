# C-file-operate

#### 介绍
本项目主要总结记录**C语言文件操作**的实用例程

#### 开发环境

本项目在如下环境中测试通过：

- Win10系统
- Dev C++ 开发环境

#### 项目结构

##### 01fwrite-test

[文件写入测试](01fwrite-test/main.c)

```c
/*打开*/
FILE *fp = fopen("../test1.bin","wb");

/*写入*/
size_t cnt = fwrite(pData, sizeof(int), DATA_SIZE, fp);

/*关闭*/
fclose(fp);
```



##### 02fread-test

[文件读取测试](02fread-test/main.c)

```c
/*读取*/
size_t cnt = fread(pData, sizeof(int), DATA_SIZE, fp);
```



##### 03fwrite-read-structData

[文件读写结构体测试](03fwrite-read-structData/main.c)

```c
BOOK book1 = {
    .name = "C语言基础",
    .page = 320,
	.price = 25.0,
};

/*写入*/
size_t cnt1 = fwrite(&book1, 1, sizeof(BOOK), fp1);
```



##### 04fwrite-read-para-test

[文件读写时的参数测试](04fwrite-read-para-test/main.c)

```c
//fp = fopen("../test-para.bin", "wb"); /*这种方式读文件失败*/ 
fp = fopen("../test-para.bin", "wb+"); /*这种方式读文件成功*/ 
```



##### 05fputs-gets-seek-test

[其它文件相关的函数测试：fputs、fgets、fseek](05fputs-gets-seek-test/main.c)

```c
/*写入*/
fputs("hello world\n", fp);/*先写入一段信息*/
fseek( fp, 6, SEEK_SET );/*读写位置从开头向后移动6个位置*/
fputs("xxpcb.github.io\n", fp);/*再写入一段信息*/
if(0 != fputs("码农爱学习\n", fp));/*再写入一段信息*/
```



#### 配套文章

- [C文件操作1：如何写入读取？fopen的6种组合参数怎么用？](https://zhuanlan.zhihu.com/p/369410100)
- [C文件操作2：如何随机的进行文件读取？](https://zhuanlan.zhihu.com/p/370527390)



#### 推荐学习

- 我的B站：<https://space.bilibili.com/146899653>
- 个人博客：<https://xxpcb.gitee.io>
- 知乎：<https://www.zhihu.com/people/xxpcb>
- CSDN：https://blog.csdn.net/hbsyaaa
- 微信公众文章：![](../pic/wxgzh.png)

感谢支持~

