# C-test

#### 介绍
本项目主要总结记录**C语言操作**的实用例程

#### 开发环境

本项目在如下环境中测试通过：

- Win10系统
- Dev C++ 开发环境

#### 项目结构

#####  C-Base64-Test

[熟悉Base64编解码的基本原理](C-Base64-Test/README.md)

##### C-file-operate

[C语言文件操作的学习记录](C-file-operate/README.md)

#### 推荐学习

- 我的B站：<https://space.bilibili.com/146899653>
- 个人博客：<https://xxpcb.gitee.io>
- 知乎：<https://www.zhihu.com/people/xxpcb>
- CSDN：https://blog.csdn.net/hbsyaaa
- 微信公众文章：![](./pic/wxgzh.png)

感谢支持~

